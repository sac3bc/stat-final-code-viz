function [estBias, estRmse, ntrialVec] = p03RepeatedM(ntrials,csvfile,r,plotsOn)
%p03RepeatedM(ntrials, csvfile, r, plotsOn): Uses the pi i method to
%calculate the area for the group of trees
    %INPUTS: 
        %ntrials: the number of trials
        %csvfile: the file to be used
        %r: the radius
        %plotsOn: whether or not to plot
    %Outputs:
        %estBias: vector with the estimated bias for the trials
        %estRmse: vector with the estimated rmse for the trials
        %ntrialVec: vector with trial numbers
        
%intialize n samples, and calculate given the correct multiples
nSamples = 0;

for i=1: ntrials
    if ceil(ntrials / i) > 50 
        nSamples = nSamples + 1; 
    end
end

%preallocate the vectors 
ntrialVec = [nSamples:nSamples:ntrials]; 
estBias = zeros(1, length(ntrialVec)); 
estRmse = zeros(1, length(ntrialVec)); 

tbaVector = zeros(1, ntrials); 

tTrue = 311.906;
counter = 1;  

perSample = 1:1:10; 

%open the document, and assign values to the relevant variables
doc = csvread(csvfile, 1, 0); 

ba = doc(:, 3);
x = doc(:, 5); 
y = doc(:, 6); 



plots = ones(1, ntrials); 


%for each trial 
for i=1: ntrials
    %calculate the x and y center
    xc = (750 + 2 * r) * rand(1) - r;  
    yc = (750 + 2 * r) * rand(1) - r; 

    
    logicalArray = ((xc - x).^2  + (yc - y).^2) < r^2; %returns an array
    
    %calculate the basal area and add to the vector 
    tHat = sum(ba(logicalArray));
    tHat = tHat * ((750^2) / (pi * (r^2))); 
    tbaVector(i) = tHat; 
    newR = r; %we save the radius as another variable so it updates in our while loop
    
   
    while (pi*(newR^2)) > overlapArea(xc, yc, newR)
        %repeat recalculating the area until it does not overlap the
        %boundary
      thisArea = (pi * (newR^2))  - overlapArea(xc, yc, newR); 
      
      newR = sqrt(thisArea / pi); 
      
      xc = (750 + 2 * newR) * rand(1) - newR;  
      yc = (750 + 2 * newR) * rand(1) - newR;
     
      
      logicalArray = ((xc - x).^2  + (yc - y).^2) < newR^2;
      tHat = tHat + (750^2 / (pi * (r^2)) * sum(ba(logicalArray)));
      tbaVector(i) = tHat; 
      
      %increment the amount of plots made by 1 
      plots(i) = plots(i) + 1; 
      
    end
 
    perSample(plots(i)) = perSample(plots(i)) + 1; 
    
        if rem(i, nSamples) == 0 %call mean() and call var()
        %use counter to index into array. look at hw 6 solution 
        ntrialVec(counter) = i;
        %t true
        %get up to tbaVector at iteration i 
        bias = 100 * (mean(tbaVector(1:i))- tTrue)/tTrue;
        %bias should be close to zero, rmse near 50
        estBias(counter) = bias;  
        rmse = 100 * sqrt(var(tbaVector(1:i)))/ tTrue; 
        estRmse(counter) = rmse; 
        counter = counter + 1; 
        end
        
end
    
%if plotsOn, plot the estimated  Bias and Rmse
if strcmp(plotsOn, 'on')
   
    figure 
    plot(ntrialVec, estBias, 'b*-'); 
    title(sprintf('Percentage Bias of TBA Estimate Using Repeated Masayuma Method\n ntrials = %5.2e', ntrials));
    xlabel('Number of Trials');
    ylabel('Percentage of Bias of Estimate');
    
    saveas(gcf, ['RepeatedMasayumaBias_',r], 'pdf');
    
    grid
    
    figure 
    plot(ntrialVec, estRmse, 'b*-'); 
    title(sprintf('Percentage RMSE of TBA Estimate Using Repeated Masayuma Method\n ntrials = %5.2e', ntrials));
    xlabel('Number of Trials');
    ylabel('Percentage RMSE of Estimate');
    
    
    grid
    
    
    saveas(gcf, ['RepeatedMasayumaRmse_',r], 'pdf');
    x = 1:1:10;
    figure 
    bar(x, perSample); 
    title(sprintf('Number of Samples Versus Number of Plots Using the Repeated Masayumas Methods\n ntrials = %5.2e', ntrials));
    xlabel('Number of Plots per Sample');
    ylabel('Number of Samples')
    grid 
    
    
    saveas(gcf, ['RepeatedMasayumaSamplesvPlots_',r], 'pdf');
end 

end
       
 
    
    
    


