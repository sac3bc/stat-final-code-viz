function [area]=overlapArea(x,y,r) 
% "overlapArea(xt,yt,rl)" is a function that computes the intersection of
% a disk of radius rl centered at (x,y) with the 750-by-750 region that
% contains the stand of trees.
% Inputs:
%   x = x coordinate of plot center
%   y = y coordinate of plot center
%   r = radius of plot 
% Ouputs: 
%   area = area of overlap between plot and tree stand

% Initialize relevant variables

area = 0; 

% Use an if-elseif conditional to check for the the following
% conditions for plot overlap in sequence and perform the appropriate calculation


     % plot completely interior to stand COMPLETELY INSIDE
     if (x-r > 0) && (x+r < 750) && (y-r > 0) && (y+r < 750) %took out the equals ex >= <= to test
         
         area = pi * r^2; 
        % disp('completely inside'); 
     
    %pi r squared
     % plot contains lower left corner (CLOSER TO CORNER /d THAN RADIUS/r)
     
     elseif (x - 0)^2  + (y-0)^2 < r^2
         
         x1 = x + sqrt(r^2 - (y-0)^2); 
         y1 = y + sqrt(r^2 - (x-0)^2); 
         h= r - sqrt((x - (x1/2))^2 + (y - (y1)/2)^2); 
         triArea = .5 * x1  * (y1)
         otherArea = r^2 * acos((r-h)/r) - ((r-h) * sqrt((2*r*h) - h^2));
         area = triArea + otherArea; 
         disp('LL corner');
     
     % plot contains upper left corner, 
     elseif (x-0)^2 + (750-y)^2 < r^2
         
        
         x1 = x + sqrt(r^2 - (750-y)^2);
         y1 = y - sqrt(r^2 - (0-x)^2); 
      
         h= r - sqrt((x - (x1/2))^2 + (y - (750+y1)/2)^2); 
         
         triArea = .5 * x1  * ( 750 - y1);
         otherArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
         area = triArea + otherArea; 
         disp('UL corner'); 
         
     % plot contains upper right corner
     elseif (750 - x)^2 + (750-y)^2 < r^2
         
         
         x1 = x - sqrt(r^2 - (750-y)^2);
         y1 = y - sqrt(r^2 - (750-x)^2); 
      
         h= r - sqrt((x - ((750 + x1)/2))^2 + (y - (750+y1)/2)^2); 
         
         triArea = .5 * (750 - x1)  * ( 750 - y1);
         otherArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
         area = triArea + otherArea; 
         
         
         disp('UR corner');
         
     % plot contains lower right corner
     elseif (750-x)^2 + (y-0)^2 < r^2
         
         
         x1 = x - sqrt(r^2 - (y-0)^2);
         y1 = y + sqrt(r^2 - (750-x)^2); 
      
         h= r - sqrt((x - ((750 + x1)/2))^2 + (y - (y1)/2)^2); 
         
         triArea = .5 * (750 - x1)  * (y1);
         otherArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
         area = triArea + otherArea; 
         
    
         
         
         
         disp('LR corner')
     % Now checking if plot inside corner but overlaps two boundaries
        %CHECKING OPPOSIT + r > x and r > y, make sure to check if its
        %750-y instead of y or 750 -x instead of x....
        
        %all 4 below are overlapping, put after each of these
    
         % plot inside lower left corner
     elseif (x - r < 0) && (x > 0) && (y > 0) && (y-r < 0)
         
         h1 = r - y;
         
         sideArea1 = r^2 * acos((r-h1)/r) - (r-h1) * sqrt((2*r*h1) - h1^2);
         
         h2 = r - x; 
         
         sideArea2 = r^2 * acos((r-h2)/r) - (r-h2) * sqrt((2*r*h2) - h2^2);
         
         area = (pi * (r^2)) - sideArea1 - sideArea2; 
         
       
        % plot inside upper left corner 
     elseif (x - r < 0) && (x > 0) && (y < 750) && (y+r > 750)
         
         h1 = r - (750-y); 
          sideArea1 = r^2 * acos((r-h1)/r) - (r-h1) * sqrt((2*r*h1) - h1^2);
          
          h2 = r - x; 
          
         sideArea2 = r^2 * acos((r-h2)/r) - (r-h2) * sqrt((2*r*h2) - h2^2);
         
         area = (pi * (r^2)) - sideArea1 - sideArea2; 
         
         
         
         
         
         % plot inside upper right corner
    elseif (x + r > 750) && (x < 750) && (y < 750) && (y+r > 750)
         h1 = r - (750 - y);  %above top 
        
         sideArea1 = r^2 * acos((r-h1)/r) - (r-h1) * sqrt((2*r*h1) - h1^2);
         
         
         h2 = r - (750 - x);  
        
         sideArea2 = r^2 * acos((r-h2)/r) - (r-h2) * sqrt((2*r*h2) - h2^2);
         
         area = (pi * (r^2)) - sideArea1 - sideArea2; 
         
         %same thing for x
         
         
         
        
        % plot inside lower right corner
     elseif (x  + r > 750) && (x < 750) &&  (y > 0) && (y-r < 0)%(y  > 0)  && (y+r < 0) %chng greater 
        
          h1 = r - y;
         
         sideArea1 = r^2 * acos((r-h1)/r) - (r-h1) * sqrt((2*r*h1) - h1^2);
       
         
          h2 = r - (750 - x);  
        
         sideArea2 = r^2 * acos((r-h2)/r) - (r-h2) * sqrt((2*r*h2) - h2^2);
         
         area = (pi * (r^2)) - sideArea1 - sideArea2;
      
         
         % Now checking if plot inside or outside a boundary
     %SIDE CONDITIONS CHECK IF OUTSIDE, IF TOUCHES
     %REG AREA - 2 SIDES CASES 
    
         % plot left of left side
    elseif (x < 0) && (x + r > 0) && (y > 0) && (y < 750)
        h = r - x;  
        
        sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
        area = (pi * (r^2)) - sideArea; 
        
       % disp('LEFT LEFT'); 
         
        % plot right of left side  
    elseif (x > 0) && (x - r < 0) && (y > 0) && (y < 750)  %maybe??
        h = r-x;  
        
        sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
        area = (pi * (r^2)) - sideArea; 
        
       % disp('RIGHT LEFT'); 
         
        % plot left of right side 
    elseif (x < 750)   && (x + r > 750) && (y > 0) && (y < 750) %maybe
        
        h = r- (750-x);
        
        sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
        area = (pi * (r^2)) - sideArea; 
        
        
       % disp('LEFT RIGHT'); 
        
        % plot right of right side 
    elseif (x > 750) &&  (x - r < 750) && (y > 0) && (y < 750)
        
        h = r - (750-x); 
        sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
        area = (pi * (r^2)) - sideArea; 
        
       
      % disp('RIGHT RIGHT'); 
        % plot below bottom side
    elseif (y < 0) && (y + r > 0) && (x > 0) && (x < 750)
        
         h = r - y;  
        
        sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
        area = (pi * (r^2)) - sideArea; 
    
       % disp('BELOW BOTTOM'); 
       
        % plot above bottom side  
   elseif (y > 0) && (y - r < 0) && (x > 0) && (x < 750)
       
        h = r - y;  
        
        sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
        area = (pi * (r^2)) - sideArea; 
        
      
      %disp('ABOVE BOTTOM'); 
    
      % plot below top side
   elseif (y < 750) && (y + r > 750) && (x > 0) && (x < 750)
    
   h = r - (750 - y);  
        
   sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
    area = (pi * (r^2)) - sideArea; 
        
   % disp('BELOW TOP'); 
    
    % plot above top side
   elseif (y > 750) && (y -r < 750) && (x > 0) && (x < 750)
       
    h = r - (750 - y);  
        
   sideArea = r^2 * acos((r-h)/r) - (r-h) * sqrt((2*r*h) - h^2);
    area = (pi * (r^2)) - sideArea; 
     % If no condition above met, plot does not overlap stand
       % disp('ABOVE TOP'); 
     else 
         
         area = 0;
     end 
    
end