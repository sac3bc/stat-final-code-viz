function [estBias, estRmse, ntrialVec] = p02MeasurePi(ntrials,csvfile,r,plotsOn)
%p02MeasurePi(ntrials, csvfile, r, plotsOn): Uses the pi i method to
%calculate the area for the group of trees
    %INPUTS: 
        %ntrials: the number of trials
        %csvfile: the file to be used
        %r: the radius
        %plotsOn: whether or not to plot
    %Outputs:
        %estBias: vector with the estimated bias for the trials
        %estRmse: vector with the estimated rmse for the trials
        %ntrialVec: vector with trial numbers
        
%intialize n samples, and calculate given the correct multiples
nSamples = 0;

for i=1: ntrials
    if ceil(ntrials / i) > 50 
        nSamples = nSamples + 1; 
    end
end


%pre-allocate your vectors 
ntrialVec = [nSamples:nSamples:ntrials]; 
estBias = zeros(1, length(ntrialVec)); 
estRmse = zeros(1, length(ntrialVec)); 

tbaVector = zeros(1, ntrials); 
%preallocate the coefficients
A = 750^2; 
tTrue = 311.906;
%counter to be used to index through vector
counter = 1;  

%open the file and get the appopriate columns
doc = csvread(csvfile, 1, 0); 

ba = doc(:, 3);
x = doc(:, 5); 
y = doc(:, 6); 

[trees, notTrees] = size(doc); 

oA = zeros( trees,1); 

%for the number of trees in the file, calculate their overlap area
for i=1: trees
    oA(i) = overlapArea(doc(i,5), doc(i, 6), r); 
 
end

    


%for each trial
for i=1: ntrials
    %calculate the x and y centers
    xc = rand(1) * 750;  
    yc = rand(1) * 750; 
   
    %calculate y 
    logicalArray = ((xc - x).^2  + (yc - y).^2) < r^2; %returns an array
    
    %calculate the appropriate tbA and insert it into the vector
    tHat = sum(ba(logicalArray)./oA(logicalArray)); 

    tHat = tHat * 750^2; 
    tbaVector(i) = tHat; 
    
    
    %if its a multpile, calculate bias, rmse and insert into vector
     if rem(i, nSamples) == 0 
        ntrialVec(counter) = i;
        %t true
        %get up to tbaVector at iteration i 
        bias = 100 * (mean(tbaVector(1:i))- tTrue)/tTrue;
        %bias should be close to zero, rmse near 50
        estBias(counter) = bias;  
        rmse = 100 * sqrt(var(tbaVector(1:i)))/ tTrue; 
        estRmse(counter) = rmse; 
        counter = counter + 1; 
     end 
    
    
end

if strcmp(plotsOn, 'on')
   
    figure 
    plot(ntrialVec, estBias, 'b*-'); 
    title(sprintf('Percentage Bias of TBA Estimate Using Measure Pi Method\n ntrials = %5.2e', ntrials));
    xlabel('Number of Trials');
    ylabel('Percentage of Bias of Estimate');
    
    saveas(gcf, ['MeasurePiBias_',r], 'pdf');
    
    grid
    
    figure 
    plot(ntrialVec, estRmse, 'b*-'); 
    title(sprintf('Percentage RMSE of TBA Estimate Using Measure Pi Method\n ntrials = %5.2e', ntrials));
    xlabel('Number of Trials');
    ylabel('Percentage Rmse of Estimate');
    
    grid
    
    
    saveas(gcf, ['MeasurePiRmse_',r], 'pdf');
    
end
    
  
 

end
