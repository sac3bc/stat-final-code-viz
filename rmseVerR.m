function [ ] = rmseVerR( ntrials,tbaMethod )
%rmseVerR: Produces a graph of % rmse given different radius values 
%INPUTS:
    %ntrials: the number of trials
    %tbamethod: the statitiscal method to calculate area 
%OUTPUTS: 
    %a graph that plots the radius to the area

%Include relevent information to be passed through the function for the
%file
csvfile='trees.csv';
plotsOn = 'off'; 
%create a vector of plot Radius and % rmse to graph 
plotRadius = (5:5:50); 
pRmse = zeros(1, 10);
%use the counter to index through the vector
counter = 1; 

%for each interval of 5, from 5 to 50
for i=5:5:50
  %call the method so you have the rmseVector
  [Bias, rmseVec, ntrialVector]  = tbaMethod(ntrials, csvfile, i, plotsOn);
  %insert the very last input into the vector 
  pRmse(counter) = rmseVec(end); 
  %update the counter
  counter = counter + 1; 
  
end

%create a plot of the information 
plot(plotRadius, pRmse, 'b*-'); 
title(sprintf('Percentage RMSE of TBA Estimate Using Masayumas Method versus Plot Radius\n ntrials = %5.2e', ntrials));
    
    xlabel('Plot Radius');
    ylabel('Percentage of RMSE of Estimate');
    grid


end
