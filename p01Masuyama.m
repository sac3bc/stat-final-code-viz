function [estBias, estRmse, ntrialVec] = p01Masuyama(ntrials,csvfile,r,plotsOn)
%p01Masayuma(ntrials, csvfile, r, plotsOn): Uses the pi i method to
%calculate the area for the group of trees
    %INPUTS: 
        %ntrials: the number of trials
        %csvfile: the file to be used
        %r: the radius
        %plotsOn: whether or not to plot
    %Outputs:
        %estBias: vector with the estimated bias for the trials
        %estRmse: vector with the estimated rmse for the trials
        %ntrialVec: vector with trial numbers
        
%intialize n samples, and calculate given the correct multiples
nSamples = 0;

for i=1: ntrials
    if ceil(ntrials / i) > 50 
        nSamples = nSamples + 1; 
    end
end


%pre-allocate your vectors 
ntrialVec = [nSamples:nSamples:ntrials]; 
estBias = zeros(1, length(ntrialVec)); 
estRmse = zeros(1, length(ntrialVec)); 

tbaVector = zeros(1, ntrials); 

Astar = (750 + (2*r))^2;  
tTrue = 311.906;
counter = 1;  

a = (pi) *  r^2; 
coeff = Astar/a; 
%open the document
doc = csvread(csvfile, 1, 0); 
%give additional arguments to SKIP first row

%get the area, x, and y columns. rename xTree, yTree
ba = doc(:, 3);
x = doc(:, 5); 
y = doc(:, 6); 

%use regular random command

%for each trial 
for i=1: ntrials
    %get random xcenter and y center
    xc = (750 + 2 * r) * rand(1) - r;  
    yc = (750 + 2 * r) * rand(1) - r;  
    
    logicalArray = ((xc - x).^2  + (yc - y).^2) < r^2; %returns an array
    
    %calculate the basal array, and add it ot he vector
    tHat = sum(ba(logicalArray)); 
    tHat = tHat * coeff; 
    tbaVector(i) = tHat; 
    
    %if its a multiple, add to estBias and estRmse vectors
    if rem(i, nSamples) == 0
        ntrialVec(counter) = i;
        
        bias = 100 * (mean(tbaVector(1:i))- tTrue)/tTrue;
      
        estBias(counter) = bias;  
        rmse = 100 * sqrt(var(tbaVector(1:i)))/ tTrue; 
        estRmse(counter) = rmse; 
        counter = counter + 1; 
        
    end
end


 %if plotsOn, plot the estimated Bias and estimated Rmse   
if strcmp(plotsOn, 'on')
    
    figure
    plot(ntrialVec, estBias, 'b*-'); 
    title(sprintf('Percentage Bias of TBA Estimate Using Masayumas Method\n ntrials = %5.2e', ntrials));
    xlabel('Number of Trials');
    ylabel('Percentage Bias of Estimate');
    
    
    grid
    
    
    saveas(gcf, ['MasayumasBias_',r], 'pdf');
    
    figure 
    plot(ntrialVec, estRmse, 'b*-'); 
    title(sprintf('Percentage RMSE of TBA Estimate Using Masayumas Method\n ntrials = %5.2e', ntrials));
    xlabel('Number of Trials');
    ylabel('Percentage Rmse of Estimate');
    
    grid
    
    saveas(gcf, ['MasayumasRmse_',r], 'pdf');
 
    
end


% use csvread to read in file
% one col. is x coord, one is y cooord, one is b.a.


end